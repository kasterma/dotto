(ns dotto.core
  (:require [clojure.string :as string]
            [clojure.set :as set]
            [clojure.data.json :as json]
            [clojure.math.combinatorics :as combinat]
            [clojure.tools.cli :as cli]
            [clojure.tools.trace :as tt]
            [taoensso.timbre :as timbre
             :only (trace debug info warn error fatal spy)]
            [taoensso.timbre.profiling :as profiling :only (p profile)]
            [clojure.core.reducers :as r])
  (:use [midje.sweet]))

;; # Dotto: a graphviz dot library

(defn dot-group [group]
  (str " { " group " } "))

(defn quote-str [i]
  (str "\"" i "\""))

(defn dot-nodes [graph]
  "create the string representation for the nodes of the graph"
  (clojure.string/join (map #(str (quote-str (:name %)) ";\n") graph)))

(def Grr1 #{{:name :one :neighbors #{:two}}
          {:name :two :neighbors #{:one :two}}
          {:name :three :neighbors #{}}})


(defn dot-edges [graph]
  "create the string representation for the edges of the graph"
  (clojure.string/join
   (map #(clojure.string/join (map (fn [nbd] (str  (quote-str (:name %)) " -> " (quote-str nbd) ";\n"))
                    (:neighbors %)))
        graph)))

(defn dot [graph]
  "return a string that contains a dot representation of the graph"
  (str "digraph G "
       (dot-group (str (dot-nodes graph)
                       (dot-edges graph)))))

(defn dotto
  [{:keys [name vertices nbdmap]}]
  (let [name-nodes (str "digraph " name "{\n"
                        (clojure.string/join ";\n" vertices)
                        ";\n")
        edges-from (fn [from to-set]
                     (reduce str "" (map (fn [to]
                                           (str from " -> " to ";\n")) to-set)))
        edges      (reduce str "" (map (fn [[k v]] (edges-from k v)) nbdmap))]
    (str name-nodes edges "}\n")))
