(ns dotto.core-test
  (:use midje.sweet
        dotto.core))

(def G1 #{{:name :one :neighbors #{:two}}
          {:name :two :neighbors #{:one :two}}
          {:name :three :neighbors #{}}})

(def G2 #{{:name "hi" :neighbors #{"ho" "howdy"}}
          {:name "ho" :neighbors #{"hi" "ho" "howdy"}}
          {:hame "howdy" :neighbors #{"howdy"}}})

(def g1
  "example graph 1"
  {:name "graph1"
   :vertices #{1 2 3}
   :nbdmap {1 #{2 3} 2 #{3} 3 #{1}}})


(facts  "to dot file"
  (dot-nodes G1) =>
  "\":two\";\n\":one\";\n\":three\";\n"                            ; fragile

  (dot-edges G1) =>
  "\":two\" -> \":one\";\n\":two\" -> \":two\";\n\":one\" -> \":two\";\n")     ; fragile


(defn testt []
  (spit "test.dot" (dot G1))
  (print (dot G1))
  (spit "testfile.dot" (dotto g1)))
