(defproject dotto "0.1.0-SNAPSHOT"
  :description "dealing with (graphviz) dot files"
  :url "https://github.com/kasterma/dotto"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.5.0"]
                 [org.clojure/data.json "0.2.1"]
                 [org.clojure/math.combinatorics "0.0.3"]
                 [org.clojure/tools.cli "0.2.2"]
                 [midje "1.5.0" :exclusions [org.clojure/clojure]]
                 [org.clojure/tools.trace "0.7.5"]
                 [com.taoensso/timbre "1.5.2"]]
  :plugins [[lein-midje "3.0.0"]])
